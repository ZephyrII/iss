 <!DOCTYPE html>
<html lang="en">
    <head>
        <title>Fuzzy</title>
      <link href='https://fonts.googleapis.com/css?family=Raleway:400, 600' rel='stylesheet' type='text/css'>
  <link rel='stylesheet' href='bitnami.css'/>
   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="header">
      <div class="container">
        <ul class="nav">
            <li><a href="/">Start</a></li>
            <li><a href='http://fc.put.poznan.pl/index.php'>Wydział Informatyki PP</a></li>
        </ul>
      </div>
    </div>

    <div class="jumbotron">
      <div class="container">  
        <div class="main">
          <p>Witaj w programie, który policzy jak wysokie jest prawdopodobieństwo, że dostaniesz się na Politechnikę, zostaniesz inżynierem i będziesz kimś!</p>

          <a href="#2" class="btn-main">Dowiedz się wiecej</a>
   
      </div>
      </div>
    </div>

    <div class="supporting" id="2">
      <div class="container">
    <p>Wszystko co musisz zrobić to tylko ocenić swoje zaangażowanie w przygotowanie do matury z matematyki i informatyki lub fizyki:</p>
		<form method="POST" action="fuzzy.php">  
			<div class="form-group"><label for="input1">Ilość dni w tygodni w tygodniu spędzonych nad zadaniami:</label>
            <input id="input1" name="input1" type="range" min="0" max="7" step="0.5" onchange="printValue('input1','inputValue1')"/> 
			<input id="inputValue1" class="inputdisplay" disabled type="text" size="2"/></div>

            <div class="form-group"><label for="input2">Średnia ocen z matematyki, fizyki i informatyki w tym semestrze:</label>
            <input id="input2" name="input2" type="range" min="0" max="6" step="0.1" onchange="printValue('input2','inputValue2')"/> 
			<input id="inputValue2" type="text" class="inputdisplay" disabled size="2"/></div>
            
            <input type="submit" name="submit" class="btn-main" value="Calculate">
            
            </form> 
	<script>
		function printValue(sliderID, textbox) {
			var x = document.getElementById(textbox);
			var y = document.getElementById(sliderID);
			x.value = y.value;
		}    
		window.onload = function() { printValue('input1', 'inputValue1'); printValue('input2', 'inputValue2'); }
	</script>
          </div>
      <div class="clearfix"></div>
    </div>

    <div class="footer">
      <div class="container">
        <p>&copy; Politechnika Poznańska 2017</p>
          <p> Natalia Graczyk, Tomasz Nowak, Adam Rogalski</p>
      </div>
      </div>
</body>
</html>


