﻿#!/usr/bin/env python
"""
==================================
The Tipping Problem - The Hard Way
==================================

 Note: This method computes everything by hand, step by step. For most people,
 the new API for fuzzy systems will be preferable. The same problem is solved
 with the new API `in this example <./plot_tipping_problem_newapi.html>`_.

The 'tipping problem' is commonly used to illustrate the power of fuzzy logic
principles to generate complex behavior from a compact, intuitive set of
expert rules.

Input variables
---------------

A number of variables play into the decision about how much to tip while
dining. Consider two of them:

* ``quality`` : Quality of the food
* ``service`` : Quality of the service

Output variable
---------------

The output variable is simply the tip amount, in percentage points:

* ``tip`` : Percent of bill to add as tip


For the purposes of discussion, let's say we need 'high', 'medium', and 'low'
membership functions for both input variables and our output variable. These
are defined in scikit-fuzzy as follows

"""
import sys, getopt
import time
import numpy as np
import skfuzzy as fuzz
import matplotlib.pyplot as plt
inputValue1 = 2.0
inputValue2 = 3.0
now = time.time()
if len(sys.argv)<2:
	print ('plot_tipping_problem.py <input1> <input2>')
	sys.exit(2)
	
inputValue1 = float(sys.argv[1])
inputValue2 = float(sys.argv[2])

# Generate universe variables
#   * Quality and service on subjective ranges [0, 10]
#   * Tip has a range of [0, 25] in units of percentage points
x_qual = np.arange(0, 11, 0.1)
x_serv = np.arange(0.0, 7.0, 0.1)
x_tip  = np.arange(0, 101, 1)

# Generate fuzzy membership functions
qual_lo = fuzz.sigmf(x_qual, 2.5, -3.0)
qual_md = fuzz.gaussmf(x_qual, 5.0, 2.0)
qual_hi = fuzz.sigmf(x_qual, 7.5, 3.0)
serv_lo = fuzz.sigmf(x_serv, 2.0, -3.0)
serv_md = fuzz.gaussmf(x_serv, 3.0, 1.0)
serv_hi = fuzz.sigmf(x_serv, 4.0, 3.0)
tip_lo = fuzz.sigmf(x_tip, 25, -0.3)
tip_md = fuzz.gaussmf(x_tip, 50, 20)
tip_hi = fuzz.sigmf(x_tip, 75, 0.3)

# Visualize these universes and membership functions
fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, figsize=(8, 9))

ax0.plot(x_qual, qual_lo, 'b', linewidth=1.5, label='Malo')
ax0.plot(x_qual, qual_md, 'g', linewidth=1.5, label='Srednio')
ax0.plot(x_qual, qual_hi, 'r', linewidth=1.5, label='Duzo')
ax0.set_title('Czas poswiecony na nauke')
ax0.legend()

ax1.plot(x_serv, serv_lo, 'b', linewidth=1.5, label='Niska')
ax1.plot(x_serv, serv_md, 'g', linewidth=1.5, label='Srednia')
ax1.plot(x_serv, serv_hi, 'r', linewidth=1.5, label='Wysoka')
ax1.set_title('Srednia ocen z przedmiotow')
ax1.legend()

ax2.plot(x_tip, tip_lo, 'b', linewidth=1.5, label='Male')
ax2.plot(x_tip, tip_md, 'g', linewidth=1.5, label='Srednie')
ax2.plot(x_tip, tip_hi, 'r', linewidth=1.5, label='Wysokie')
ax2.set_title('Szanse dostania sie na Politechnike')
ax2.legend()

#Turn off top/right axes
for ax in (ax0, ax1, ax2):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

plt.tight_layout()
filename = "plots/1.png"
print (filename)
plt.savefig(filename)
#plt.show()

"""
.. image:: PLOT2RST.current_figure

Fuzzy rules
-----------

Now, to make these triangles useful, we define the *fuzzy relationship*
between input and output variables. For the purposes of our example, consider
three simple rules:

1. If the food is bad OR the service is poor, then the tip will be low
2. If the service is acceptable, then the tip will be medium
3. If the food is great OR the service is amazing, then the tip will be high.

Most people would agree on these rules, but the rules are fuzzy. Mapping the
imprecise rules into a defined, actionable tip is a challenge. This is the
kind of task at which fuzzy logic excels.

Rule application
----------------

What would the tip be in the following circumstance:

* Food *quality* was **6.5**
* *Service* was **9.8**

"""

# We need the activation of our fuzzy membership functions at these values.
# The exact values 6.5 and 9.8 do not exist on our universes...
# This is what fuzz.interp_membership exists for!
qual_level_lo = fuzz.interp_membership(x_qual, qual_lo, inputValue1)
qual_level_md = fuzz.interp_membership(x_qual, qual_md, inputValue1)
qual_level_hi = fuzz.interp_membership(x_qual, qual_hi, inputValue1)

serv_level_lo = fuzz.interp_membership(x_serv, serv_lo, inputValue2)
serv_level_md = fuzz.interp_membership(x_serv, serv_md, inputValue2)
serv_level_hi = fuzz.interp_membership(x_serv, serv_hi, inputValue2)

# Now we take our rules and apply them. Rule 1 concerns bad food OR service.
# The OR operator means we take the maximum of these two.
active_rule1 = np.fmax(qual_level_lo, serv_level_lo)

# Now we apply this by clipping the top off the corresponding output
# membership function with `np.fmin`
tip_activation_lo = np.fmin(active_rule1, tip_lo)  # removed entirely to 0

# For rule 2 we connect acceptable service to medium tipping
tip_activation_md = np.fmin(serv_level_md, tip_md)

# For rule 3 we connect high service OR high food with high tipping
active_rule3 = np.fmax(qual_level_hi, serv_level_hi)
tip_activation_hi = np.fmin(active_rule3, tip_hi)
tip0 = np.zeros_like(x_tip)

# Visualize this
fig, ax0 = plt.subplots(figsize=(7, 3))

ax0.fill_between(x_tip, tip0, tip_activation_lo, facecolor='b', alpha=0.7)
ax0.plot(x_tip, tip_lo, 'b', linewidth=0.5, linestyle='--', )
ax0.fill_between(x_tip, tip0, tip_activation_md, facecolor='g', alpha=0.7)
ax0.plot(x_tip, tip_md, 'g', linewidth=0.5, linestyle='--')
ax0.fill_between(x_tip, tip0, tip_activation_hi, facecolor='r', alpha=0.7)
ax0.plot(x_tip, tip_hi, 'r', linewidth=0.5, linestyle='--')
ax0.set_title('Dane wyjsciowe')

# Turn off top/right axes
for ax in (ax0,):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

plt.tight_layout()
filename = 'plots/2.png'
print (filename)
plt.savefig(filename)
#plt.show()

"""
.. image:: PLOT2RST.current_figure

Rule aggregation
----------------

With the *activity* of each output membership function known, all output
membership functions must be combined. This is typically done using a
maximum operator. This step is also known as *aggregation*.

Defuzzification
---------------
Finally, to get a real world answer, we return to *crisp* logic from the
world of fuzzy membership functions. For the purposes of this example
the centroid method will be used.

The result is a tip of **20.2%**.
---------------------------------
"""

# Aggregate all three output membership functions together
aggregated = np.fmax(tip_activation_lo,
                     np.fmax(tip_activation_md, tip_activation_hi))

# Calculate defuzzified result
tip = fuzz.defuzz(x_tip, aggregated, 'centroid')
tip_activation = fuzz.interp_membership(x_tip, aggregated, tip)  # for plot

# Visualize this
fig, ax0 = plt.subplots(figsize=(7, 3))

ax0.plot(x_tip, tip_lo, 'b', linewidth=0.5, linestyle='--', )
ax0.plot(x_tip, tip_md, 'g', linewidth=0.5, linestyle='--')
ax0.plot(x_tip, tip_hi, 'r', linewidth=0.5, linestyle='--')
ax0.fill_between(x_tip, tip0, aggregated, facecolor='Orange', alpha=0.7)
ax0.plot([tip, tip], [0, tip_activation], 'k', linewidth=1.5, alpha=0.9)
ax0.set_title('Sredni wynik')

# Turn off top/right axes
for ax in (ax0,):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

plt.tight_layout()
filename = 'plots/3.png'
print (filename)
plt.savefig(filename)
#plt.show()
print (tip)
"""
.. image:: PLOT2RST.current_figure

Final thoughts
--------------

The power of fuzzy systems is allowing complicated, intuitive behavior based
on a sparse system of rules with minimal overhead. Note our membership
function universes were coarse, only defined at the integers, but
``fuzz.interp_membership`` allowed the effective resolution to increase on
demand. This system can respond to arbitrarily small changes in inputs,
and the processing burden is minimal.

"""
