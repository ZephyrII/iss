 <!DOCTYPE html>
<html lang="en">
    <head>
        <title>Fuzzy</title>
      <link href='https://fonts.googleapis.com/css?family=Raleway:400, 600' rel='stylesheet' type='text/css'>
  <link rel='stylesheet' href='bitnami.css'/>
   <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  </head>
  <body>
    <div class="header">
      <div class="container">
        <ul class="nav">
            <li><a href="/">Start</a></li>
            <li><a href='http://fc.put.poznan.pl/index.php'>Wydział Informatyki PP</a></li>
        </ul>
      </div>
    </div>


<br>
<h1>Procent szans: 
<?php
if (isset($_POST['submit'])) {	
	$output = exec("RD /S /Q plots");
	$output = exec("MD plots");
	$raw_command = 'python fuzzy_python.py '.$_POST['input1'].' '.$_POST['input2'];
	$command = escapeshellcmd($raw_command);
	$output = exec($command);
	echo number_format((float)$output, 2, '.', '');
}
?>
%</h1>
 <img src="plots/1.png" style="width:500px;height:456px;display:block;">
 <img src="plots/2.png" style="width:500px;height:456px;display:block;">
 <img src="plots/3.png" style="width:500px;height:456px;">
      
 <div class="footer">
      <div class="container">
        <p>&copy; Politechnika Poznańska 2017</p>
          <p> Natalia Graczyk, Tomasz Nowak, Adam Rogalski</p>
      </div>
      </div>
</body>
</html>